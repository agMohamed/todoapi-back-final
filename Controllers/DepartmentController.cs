using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;
using todoapi_back.Filter;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentController:ControllerBase
    {
      private readonly TodoApiDbContext _context;
      public DepartmentController(TodoApiDbContext context)
      {
        _context=context;   
      }
      [HttpGet]
      public async Task<ActionResult<IEnumerable<Department>>>GetDeaprtments([FromQuery] PaginationFilter filter,[Optional]string sort ){
       
        var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

        var totalDepartments = await _context.Mohamed_Departments.CountAsync();

       var department= await  _context.Mohamed_Departments .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
            .Take(validFilter.PageSize)
            .ToListAsync();

        
        switch(sort){
          case "departmentname":
          department= (List<Department>)department.OrderBy(d=>d.DepartmentName);
          break;
          
          case "departmentname_desc":
          department= (List<Department>)department.OrderByDescending(d=>d.DepartmentName);
          break;
          case "deptcode":
          department= (List<Department>)department.OrderBy(d=>d.DeptCode);
          break;
          case "deptcode_desc":
          department= (List<Department>)department.OrderByDescending(d=>d.DeptCode);
          break;
          default:
           department= (List<Department>)department.OrderBy(d=>d.DeptCode);
          break;
        }
        return department;
      }
      [HttpGet("{id}")]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
          var department = await _context.Mohamed_Departments.FindAsync(id);
          if(department == null){
              return NotFound();
          }else{
            return department;
          }
          
        }
      //post
      [HttpPost]
      public async Task<ActionResult<Department>> PostDepartment(Department department)
      {
        if(department==null){
          BadRequest();
        }else{
          _context.Mohamed_Departments.Add(new Department{
            DeptCode=department.DeptCode,
            DepartmentName=department.DepartmentName
          });
        }
        //_context.Mohamed_Departments.Add(department);

        await _context.SaveChangesAsync();

          //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
          return Ok(new{message="Department is add successfully"});
      }

       //put : api/employee
      [HttpPut("{id}")]
      public async Task<IActionResult> PutDepartment(int id, Department department)
      {
        var vdepartment = await _context.Mohamed_Departments.FindAsync(id);
        if (vdepartment==null)
        {
          return NoContent();
        }
        vdepartment.DeptCode=department.DeptCode;
        vdepartment.DepartmentName=department.DepartmentName;
       // _context.Entry(department).State = EntityState.Modified;
        _context.Mohamed_Departments.Update(department);
        try
        {
               
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!DepartmentExists(id))
            {
              return NotFound();
            }
            else
            {
              throw;
            }
        }
        return NoContent();
      }
         //delete 
      [HttpDelete("{id}")]
      public async Task<IActionResult> DeleteDepartment(int id)
      {
            var department = await _context.Mohamed_Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            _context.Mohamed_Departments.Remove(department);
           
            
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DepartmentExists(int id)
        {
            return _context.Mohamed_Departments.Any(e => e.DeptCode == id);
        }

    }
}