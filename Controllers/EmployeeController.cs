using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TodoApi.Models;
using todoapi_back;
using todoapi_back.Filter;
using todoapi_back.Models;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController:ControllerBase
    {
        
        private readonly TodoApiDbContext _context;
       
      
        public EmployeeController(TodoApiDbContext context)
        {
            _context=context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>>GetEmployees([FromQuery] PaginationFilter filter,[Optional]string sort,[Optional]string searchString){
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

            var totalEmployees = await _context.Mohamed_Employees.CountAsync();


            var employee=await  _context.Mohamed_Employees
            .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
            .Take(validFilter.PageSize)
            .ToListAsync();

            if(!string.IsNullOrEmpty(searchString)){
                    employee= (List<Employee>)employee.Where(e=>e.Name.Equals(searchString)||
                    e.Id.Equals(searchString)||
                    e.email.Equals(searchString)||
                    e.DeptCode.Equals(searchString)||
                    e.Department.Equals(searchString));
            };

        switch(sort){

            case "":employee= (List<Employee>)employee.OrderBy(d=>d.Id);
            break;

          case "name":
          employee= (List<Employee>)employee.OrderBy(d=>d.Name);
          break;
          
          case "name_desc":
          employee= (List<Employee>)employee.OrderByDescending(d=>d.Name);
          break;
          case "Id":
          employee= (List<Employee>)employee.OrderBy(d=>d.Id);
          break;
          case "Id_desc":
          employee= (List<Employee>)employee.OrderByDescending(d=>d.Id);
          break;
          case "email":employee= (List<Employee>)employee.OrderBy(d=>d.email);
          break;
          case "email_desc":employee= (List<Employee>)employee.OrderByDescending(d=>d.email);
          break;
          default:
           employee= (List<Employee>)employee.OrderBy(d=>d.Id);
          break;
        }
            return employee;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var employee = await _context.Mohamed_Employees.FindAsync(id);
            if(employee == null){
                return NotFound();
            }

            return employee;
        }

        //Post:api/Employee
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee(EmployeeVM employeeVM)
        {
            if (ModelState.IsValid)  
            {  
                string uniqueImageName = UploadImage(employeeVM);  

                Employee employee = new Employee  
                {  
                    Id=employeeVM.Id,
                    Name = employeeVM.Name,
                    email=employeeVM.email,
                    DeptCode=employeeVM.DeptCode,
                    ImageUrl = uniqueImageName  
                };
                _context.Mohamed_Employees.Add(employee);
                 await _context.SaveChangesAsync();
            }
            
            
             await _context.SaveChangesAsync();
             return CreatedAtAction(nameof(GetEmployee), new { id = employeeVM.Id }, employeeVM);
        }    

         private string UploadImage(EmployeeVM model)  
        {  
            string uniqueFileName = null;  
  
            if (model.Profile != null)  
            {  
                string uploadsFolder = Path.Combine("Resources","Images"); 
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Profile.FileName;  
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);  
                using (var fileStream = new FileStream(filePath, FileMode.Create))  
                {  
                    model.Profile.CopyTo(fileStream);  
                }  
            }  
            return uniqueFileName;  
        }

  
        //put : api/employee
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployeee(int id,EmployeeVM employeeVM)
        {
            var vemployee= await _context.Mohamed_Employees.FindAsync(id);
             DeleteImage(vemployee.ImageUrl);
            if (vemployee==null)
            {
                return BadRequest();
            }
            string updateImageUrl = UpdateFile(employeeVM);
           // _context.Entry(employee).State = EntityState.Modified;
           

            vemployee.Id=employeeVM.Id;
            vemployee.Name=employeeVM.Name;
            vemployee.email=employeeVM.email;
            vemployee.DeptCode=employeeVM.DeptCode;
            vemployee.ImageUrl=updateImageUrl;
            _context.Mohamed_Employees.Update(vemployee);
            try
            {
                
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private string UpdateFile(EmployeeVM model)  
        {  
            string updateImageUrl = null;  
  
            if (model.Profile != null)  
            {  
                string uploadsFolder = Path.Combine("Resources","Images"); 
                updateImageUrl = Guid.NewGuid().ToString() + "_" + model.Profile.FileName;  
                string filePath = Path.Combine(uploadsFolder, updateImageUrl);  
                using (var fileStream = new FileStream(filePath, FileMode.Create))  
                {  
                    model.Profile.CopyTo(fileStream);  
                }  
            }  
            return updateImageUrl;  
        }

        private void DeleteImage(string url){
            string path=url;
             FileInfo file = new FileInfo(path);  
             if (file.Exists)//check file exsit or not  
            {  
                file.Delete();     
            }  
     

        }
        //delete 
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var employee = await _context.Mohamed_Employees.FindAsync(id);
            
            if (employee == null)
            {
                return NotFound();
            }
                DeleteImage(employee.ImageUrl);
            _context.Mohamed_Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return NoContent();
            }
            private bool EmployeeExists(int id)
            {
                return _context.Mohamed_Employees.Any(e => e.Id == id);
          }
        }
    
    }
