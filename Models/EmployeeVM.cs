using Microsoft.AspNetCore.Http;

namespace todoapi_back.Models
{
    public class EmployeeVM
    {
         public int Id{get;set;}

        public string Name{get;set;}
        public string email{get;set;}

        public int DeptCode{get;set;}

        public IFormFile Profile{get;set;}

     }
}